﻿namespace DES_v1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.start = new System.Windows.Forms.Button();
            this.Gen_key = new System.Windows.Forms.Button();
            this.decr_txt = new System.Windows.Forms.TextBox();
            this.encr_txt = new System.Windows.Forms.TextBox();
            this.key = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_encr = new System.Windows.Forms.RadioButton();
            this.radioButton_decr = new System.Windows.Forms.RadioButton();
            this.unload_file = new System.Windows.Forms.Button();
            this.load_file = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(666, 335);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(123, 51);
            this.start.TabIndex = 0;
            this.start.Text = "Выполнить";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // Gen_key
            // 
            this.Gen_key.Location = new System.Drawing.Point(32, 380);
            this.Gen_key.Name = "Gen_key";
            this.Gen_key.Size = new System.Drawing.Size(123, 48);
            this.Gen_key.TabIndex = 1;
            this.Gen_key.Text = "Сгенерировать ключ";
            this.Gen_key.UseVisualStyleBackColor = true;
            this.Gen_key.Click += new System.EventHandler(this.Gen_key_Click);
            // 
            // decr_txt
            // 
            this.decr_txt.Location = new System.Drawing.Point(32, 33);
            this.decr_txt.Multiline = true;
            this.decr_txt.Name = "decr_txt";
            this.decr_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.decr_txt.Size = new System.Drawing.Size(337, 257);
            this.decr_txt.TabIndex = 2;
            // 
            // encr_txt
            // 
            this.encr_txt.Location = new System.Drawing.Point(487, 33);
            this.encr_txt.Multiline = true;
            this.encr_txt.Name = "encr_txt";
            this.encr_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.encr_txt.Size = new System.Drawing.Size(337, 257);
            this.encr_txt.TabIndex = 3;
            // 
            // key
            // 
            this.key.Location = new System.Drawing.Point(32, 326);
            this.key.Name = "key";
            this.key.Size = new System.Drawing.Size(120, 22);
            this.key.TabIndex = 4;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_encr);
            this.groupBox1.Controls.Add(this.radioButton_decr);
            this.groupBox1.Location = new System.Drawing.Point(283, 309);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 39);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radioButton_encr
            // 
            this.radioButton_encr.AutoSize = true;
            this.radioButton_encr.Checked = true;
            this.radioButton_encr.Location = new System.Drawing.Point(179, 12);
            this.radioButton_encr.Name = "radioButton_encr";
            this.radioButton_encr.Size = new System.Drawing.Size(121, 21);
            this.radioButton_encr.TabIndex = 1;
            this.radioButton_encr.TabStop = true;
            this.radioButton_encr.Text = "Зашифровать";
            this.radioButton_encr.UseVisualStyleBackColor = true;
            // 
            // radioButton_decr
            // 
            this.radioButton_decr.AutoSize = true;
            this.radioButton_decr.Location = new System.Drawing.Point(0, 12);
            this.radioButton_decr.Name = "radioButton_decr";
            this.radioButton_decr.Size = new System.Drawing.Size(128, 21);
            this.radioButton_decr.TabIndex = 0;
            this.radioButton_decr.Text = "Расшифровать";
            this.radioButton_decr.UseVisualStyleBackColor = true;
            // 
            // unload_file
            // 
            this.unload_file.Location = new System.Drawing.Point(473, 377);
            this.unload_file.Name = "unload_file";
            this.unload_file.Size = new System.Drawing.Size(123, 51);
            this.unload_file.TabIndex = 6;
            this.unload_file.Text = "Выгрузить файл";
            this.unload_file.UseVisualStyleBackColor = true;
            this.unload_file.Click += new System.EventHandler(this.unload_file_Click);
            // 
            // load_file
            // 
            this.load_file.Location = new System.Drawing.Point(283, 380);
            this.load_file.Name = "load_file";
            this.load_file.Size = new System.Drawing.Size(123, 51);
            this.load_file.TabIndex = 7;
            this.load_file.Text = "Загрузить файл";
            this.load_file.UseVisualStyleBackColor = true;
            this.load_file.Click += new System.EventHandler(this.load_file_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 498);
            this.Controls.Add(this.load_file);
            this.Controls.Add(this.unload_file);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.key);
            this.Controls.Add(this.encr_txt);
            this.Controls.Add(this.decr_txt);
            this.Controls.Add(this.Gen_key);
            this.Controls.Add(this.start);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button Gen_key;
        private System.Windows.Forms.TextBox decr_txt;
        private System.Windows.Forms.TextBox encr_txt;
        private System.Windows.Forms.TextBox key;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_encr;
        private System.Windows.Forms.RadioButton radioButton_decr;
        private System.Windows.Forms.Button unload_file;
        private System.Windows.Forms.Button load_file;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}

