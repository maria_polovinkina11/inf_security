﻿using System;
using System.Text;
using System.Windows.Forms;

namespace DES_v1
{
    public partial class Form1 : Form
    {

       
        private const int lengtKey = 8;//длина ключа 
       

        string mas_str = "abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNORSTUYWXYZPQ0123456789";// строка символов, из которой генерируем ключ

        string value_key, // значение ключа
                  dec_txt,
                    in_txt,
                       padded_txt;
                        
        string output_txt;
        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//открываем файл
            saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//записываем текст в файл
        }
        //кнопка "Сгенерировать ключ"
        private void Gen_key_Click(object sender, EventArgs e)
        {

            value_key = GenRandomStr(mas_str, lengtKey);
            key.Text = value_key;
        }
        //кнопка "Загрузить файл"
        private void load_file_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog1.FileName;
            // читаем файл в строку
            string fileText = System.IO.File.ReadAllText(filename);
            decr_txt.Text = fileText;
            in_txt = fileText;
            MessageBox.Show("Файл открыт");
        }

      //кнопка "Выполнить"

        private void start_Click(object sender, EventArgs e)
        {
            if (radioButton_encr.Checked == true)//если  "Зашифровать"
            {
                padded_txt = padded_Text(in_txt, value_key);// дополняем текст
                dec_txt = XOR(padded_txt, value_key);// шифруем текст
                encr_txt.Text = dec_txt;// выводим зашифрованный текст в TextBox
            }
            else              // если "Расшифровать"
            {
                output_txt = decrypt(dec_txt, value_key);// расшифровываем наш текст
                decr_txt.Text=output_txt;// выводим в TextBox
            }
        }
        //кнопка "Выгрузить"
        private void unload_file_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;

            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, encr_txt.Text);

            MessageBox.Show("Файл сохранен");

        }




        //функция генерации ключа
        private string GenRandomStr(string data, int len)//генерация ключа из строки
        {
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder(len);

            int Position = 0;

            for (int i = 0; i < len; i++)
            {
                Position = rnd.Next(0, data.Length - 1);
                sb.Append(data[Position]);
            }
            return sb.ToString();
        }


        //функция для дополнения текста
        private string padded_Text(string data, string key)
        {
            int dif = ((key.Length - data.Length%key.Length)%key.Length);
            string new_data = data;
            for (int i = 0; i < dif; i++)
            {
                new_data += "\0";
            }

            return new_data;
        }
        // функция шифрования
        private string XOR(string input, string key)
        {
            // string res = "";
            var input_bin = Encoding. Unicode.GetBytes(input);
            var key_bin = Encoding.Unicode.GetBytes(key);
            var res = new byte[input_bin.Length];
            for(int i=0; i<input_bin.Length; i++)
            {
                res[i] = (byte)(input_bin[i] ^ key_bin[i % key_bin.Length]);
            }
            return Encoding.Unicode.GetString(res);
        }


        //функция расшифрования
        private string decrypt(string output, string key)
        {
             return XOR(output, key);
            
        }
    }
}






